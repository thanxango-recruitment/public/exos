"""
 max_sum() returns the maximum sum of *n* consecutive
 elements in *list*

 example:
 max_sum([3, 2, 5, 7, 4, 2, 3, 8, 4], 3) returns 16
 which is 5 + 7 + 4
"""


def max_sum(l, k):
    return 0
