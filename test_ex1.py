import json

from ex1 import max_sum


def test_case1():
    assert max_sum([3, 2, 5, 7, 4, 2, 3, 8, 4], 3) == 16


def test_case2():
    assert max_sum([1, 2, 3], 0) == 0


def test_case3():
    assert max_sum([], 0) == 0


def test_case4():
    with open("ex1.json") as fo:
        data = json.load(fo)
    for test_case in data:
        assert max_sum(test_case["tab"], test_case["k"]) == test_case["max_sum"]
