import json

from ex2 import list_comments


def test_case1():
    assert list_comments([]) == ""


def test_case2():
    assert list_comments([{"comment": "lorem"}]) == "<ul><li>lorem</li></ul>"


def test_case3():
    with open("ex2.json") as fo:
        data = json.load(fo)
    for test_case in data:
        assert list_comments(test_case["comments"]) == test_case["expected"]
