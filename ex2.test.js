const list_comments = require('./ex2');

test("test 1", () => {
    expect(list_comments([])).toBe("");
});

test("test 2", () => {
    expect(list_comments([{"comment": "lorem"}])).toBe(
        "<ul><li>lorem</li></ul>");
});


test("test 3", () => {
    data = require("./ex2.json");
    data.forEach( (test_case) => {
        expect(
            list_comments(test_case["comments"])
        ).toBe(
            test_case["expected"]
        );
    });
});
